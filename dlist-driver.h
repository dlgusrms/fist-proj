
#include "dlist.h"
#ifndef DLIST_DRIVER_H
#define DLIST_DRIVER_H

void addItemToList(int n, DLList* list);
void removeAll(int n, DLList* list);

#endif
