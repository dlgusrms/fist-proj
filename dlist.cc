#include "dlist.h"

#include "dlist-driver.h"
DLLElement::DLLElement(void *itemPtr, int sortKey)
{
	key = sortKey;
	item = itemPtr;

	next = nullptr;
	prev = nullptr;
}

void DLList::SelfTest()
{
	DLList* list = new DLList();
	addItemToList(10, list);
	removeAll(10, list);
}
DLList::DLList() 
{
	first = nullptr;
	last = nullptr;
}
DLList::~DLList()
{
	DLLElement* nextElem = first;
	while(true)
	{
		if (nextElem == nullptr)
			return;
		delete nextElem;
		nextElem = nextElem->next;
	}
}
// initialize a list element

void DLList::Prepend(void *item)
{
	DLLElement* newListElem;
	
	if (!IsEmpty())
	{
		newListElem = new DLLElement(item, NORMAL_PRIORITY);

		first = newListElem;
		last = newListElem;
	}
	else
	{
		newListElem = new DLLElement(item, first->key -1 );
		newListElem->next = first;
		first->prev = newListElem;

		first = newListElem;
	}
}

// add to head of list (set key = min_key-1) 
void DLList::Append(void *item) 
{
	DLLElement* newListElem;

	if (!IsEmpty()){

		newListElem = new DLLElement(item, NORMAL_PRIORITY);
		first = newListElem;
		last = newListElem;
	}
	else
	{
		newListElem = new DLLElement(item, last->key + 1);
		newListElem->prev = last;
		last->next = newListElem;
	}
	last = newListElem;
} 
// add to tail of list (set key = max_key+1) 
void* DLList::Remove(int* keyPtr) {
	if (!IsEmpty())
		return nullptr;

	DLLElement* removeNode = first;
	*keyPtr = removeNode->key;
	first = first->next;
	return removeNode->item;
}  
// remove from head of list
// set *keyPtr to key of the removed item
// return item (or NULL if list is empty)

bool DLList::IsEmpty() {
	if(first == nullptr)
		return false;
	else
		return true;
}             // return true if list has elements
							// routines to put/get items on/off list in order (sorted by key)

void DLList::SortedInsert(void *item, int sortKey) {
	DLLElement* node = first;
	DLLElement* newListElem = new DLLElement(item, sortKey);

	if (node == nullptr) // list is empty
	{
		first = newListElem;
		last = newListElem;
		return;
	}
	else if (sortKey <= node->key) //insert at first
	{
		first = newListElem;
		first->next = node;
		node->prev = first;
		return;
	}
	else
	{
		for (DLLElement* curNode = node; curNode != nullptr; curNode = curNode->next)
		{
			if (sortKey <= curNode->key)
			{
				DLLElement* prevNode = curNode->prev;
				
				prevNode->next = newListElem;
				curNode->prev = newListElem;
				newListElem->prev = prevNode;//->next;
				newListElem->next = curNode;

				
				return;
			}
		}

		last->next = newListElem;
		newListElem->prev = last;
		last = newListElem;
	}
}
void* DLList::SortedRemove(int sortKey) {
	DLLElement* node = first;

	if (node == nullptr)
		return nullptr;

	do{
		if (node->key == sortKey)
		{
			//remove
			if (node->prev == nullptr) // node is first node
			{
				first = node->next;
				first->prev = nullptr;
			}
			else if (node == last) // node is last node.
			{
				last = node->prev;
				last->next = nullptr;
			}
			else
			{
				node->prev->next = node->next;
				node->next->prev = node->prev;
			}

			return node;
		}
		
	} while ((node = node->next) != nullptr);

	return nullptr; 
	
}  // remove first item with key==sortKey
