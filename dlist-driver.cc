
#include "dlist-driver.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "dlist.h"

//n개의 아이템을 랜덤으로 더블링크드리스트에 저장

void addItemToList(int n, DLList* list)
{
	srand(time(NULL));

	for(int i = 0 ; i < n ; i++)
	{
		
		int key = rand() % 31 + (NORMAL_PRIORITY -15); // NORMAL_PRIORITY-15 ~ NORMAL_PRIORITY+15
		int* item = new int(rand() % 10000);

		list->SortedInsert(item,key);
	}
}
//n개의 아이템을 처음(헤드)부터 제거 콘솔에다가 출력.
void removeAll(int n, DLList* list)
{
	int key;
	for (int i = 0; i < n; i++)
	{
		void* item = list->Remove(&key);
		printf("deleted key=%d item=%d \n", key, *((int*)item));
	}
}
